﻿using AutoMapper;
using CsvImport.BoModels;
using CsvImport.CommonModels.InputModels;

using CsvImport.Service.Models;
using CsvImport.Store.Interfaces;
using EFCore.BulkExtensions;
using MssqlEfStore.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CsvImport.Store.Mssql
{
    public class MssqlStore : IStore
    {
        private MssqlDbContext _dbContext = null;
        private IMapper _mapper;

        public MssqlStore(MssqlDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task SaveAsync<T>(FileImportInfo fileinfo, IEnumerable<T> dataItems)
        {
            switch (dataItems)
            {
                case IEnumerable<RowLO> rows:
                    await SaveFileDataAsync(fileinfo, rows);
                    break;

                case IEnumerable<ProductBO> products:
                    await SaveFileDataAsync(fileinfo, products);
                    break;

                default:
                    throw new NotImplementedException();
            };
            //else
            //    throw new NotImplementedException();

            return;
        }

        protected async Task SaveFileDataAsync(FileImportInfo fileinfo, IEnumerable<ProductBO> dataItems)
        {
            IList<Product> records = new List<Product>();

            foreach (ProductBO item in dataItems)
            {
                records.Add(_mapper.Map<Product>(item));
            };

            try
            {
                await _dbContext.BulkInsertOrUpdateAsync<Product>(records);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected async Task SaveFileDataAsync(FileImportInfo fileinfo, IEnumerable<RowLO> dataItems)
        {
            IList<FileData> records = new List<FileData>();

            foreach (RowLO item in dataItems)
            {
                records.Add(new FileData()
                {
                    FileDataId = fileinfo.fileLoadId,
                    Key = item.Key,
                    ArtikelCode = item.ArtikelCode,
                    ColorCode = item.ColorCode,
                    Description = item.Description,
                    Price = item.Price,
                    DiscountPrice = item.DiscountPrice,
                    DeliveredIn = item.DeliveredIn,
                    Q1 = item.Q1,
                    Size = item.Size,
                    Color = item.Color
                });
            };

            try
            {
                await _dbContext.BulkInsertOrUpdateAsync<FileData>(records);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}