﻿namespace MssqlEfStore.Model
{
    public partial class Product
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string ArtikelCode { get; set; }
        public string Description { get; set; }
        public decimal? Price { get; set; }
        public decimal? DiscountPrice { get; set; }
        public string Q1 { get; set; }
        public string Size { get; set; }
        public int? ColorId { get; set; }
        public int? DeliveryId { get; set; }
        public int? ColorCodeId { get; set; }

        public virtual Color Color { get; set; }
        public virtual ColorCode ColorCode { get; set; }
        public virtual DeliveryTerms Delivery { get; set; }
    }
}