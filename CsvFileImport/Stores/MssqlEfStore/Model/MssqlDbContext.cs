﻿using Microsoft.EntityFrameworkCore;

namespace MssqlEfStore.Model
{
    public partial class MssqlDbContext : DbContext
    {
        public MssqlDbContext()
        {
        }

        public MssqlDbContext(DbContextOptions<MssqlDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Color> Color { get; set; }
        public virtual DbSet<ColorCode> ColorCode { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<DeliveryTerms> DeliveryTerms { get; set; }
        public virtual DbSet<FileData> FileData { get; set; }
        public virtual DbSet<Product> Product { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                //                optionsBuilder.UseSqlServer("Server=chap2\\sqlexpress;Database=TestDb;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Color>(entity =>
            {
                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<ColorCode>(entity =>
            {
                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.Property(e => e.Data1).HasColumnName("data1");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<DeliveryTerms>(entity =>
            {
                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Description).HasMaxLength(300);
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.ArtikelCode)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.DiscountPrice).HasColumnType("money");

                entity.Property(e => e.Price).HasColumnType("money");

                entity.Property(e => e.Q1).HasMaxLength(50);

                entity.Property(e => e.Size).HasMaxLength(10);

                entity.HasOne(d => d.ColorCode)
                    .WithMany(p => p.Product)
                    .HasForeignKey(d => d.ColorCodeId)
                    .HasConstraintName("FK_Product_ColorCode");

                entity.HasOne(d => d.Color)
                    .WithMany(p => p.Product)
                    .HasForeignKey(d => d.ColorId)
                    .HasConstraintName("FK_Product_Color");

                entity.HasOne(d => d.Delivery)
                    .WithMany(p => p.Product)
                    .HasForeignKey(d => d.DeliveryId)
                    .HasConstraintName("FK_Product_DeliveryTerms");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}