﻿using System.Collections.Generic;

namespace MssqlEfStore.Model
{
    public partial class DeliveryTerms
    {
        public DeliveryTerms()
        {
            Product = new HashSet<Product>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Product> Product { get; set; }
    }
}