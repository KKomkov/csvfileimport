﻿using AutoMapper;
using CsvImport.BoModels;
using MssqlEfStore.Model;

namespace MssqlEfStore.Mapping
{
    public class MssqlMappingProfile : Profile
    {
        public MssqlMappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<ColorBO, Color>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Code));

            CreateMap<ColorCodeBO, ColorCode>()
               .ForMember(dest => dest.Id, opt => opt.Ignore())
               .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Code));

            CreateMap<DeliveryBO, DeliveryTerms>()
               .ForMember(dest => dest.Id, opt => opt.Ignore())
               .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Code));

            CreateMap<ProductBO, Product>()
                .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Key))
                 .ForMember(dest => dest.Delivery, opt => opt.MapFrom(src => src.DeliveredIn));
            //.ForMember(dest => dest.ColorCode, opt => opt.Ignore())
            //.ForMember(dest => dest.Delivery, opt => opt.Ignore());
        }
    }
}