﻿using CsvImport.Service.Models;
using CsvImport.Store.Interfaces;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace CsvImport.Store.JsonStore
{
    public class JsonStore : IStore
    {
        protected IConfiguration config;

        public JsonStore(IConfiguration configuration)
        {
            config = configuration;
        }

        //Known issue: Current implementation is not thread safe
        protected void PrepareFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                using (var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite))
                {
                    //remove last symbol ']' from file
                    fileStream.Seek(-1, SeekOrigin.End);
                    if (fileStream.ReadByte() == ']')
                    {
                        fileStream.Position = (fileStream.Length - 1);
                        fileStream.WriteByte((byte)',');
                    }
                    else if (fileStream.Length == 0)
                    {
                        fileStream.WriteByte((byte)'[');
                    }

                    fileStream.Flush();
                    fileStream.Close();
                }
            }
            else
            {
                using (StreamWriter file = File.AppendText(fileName))
                {
                    file.Write('[');
                }
            }
        }

        public async Task SaveAsync<T>(FileImportInfo fileInfo, IEnumerable<T> dataItems)
        {// path file name as class input parameter
            string fileNameFormat = config.GetSection("DataStorage:JsonStorage").GetSection("FileName").Value;
            string fileName = string.Format(fileNameFormat, fileInfo.fileLoadId);

            PrepareFile(fileName);

            JsonSerializer serializer = new JsonSerializer();

            using (StreamWriter file = File.AppendText(fileName))
            {
                //as alternative serialize whole  dataItems and remove first [
                foreach (T item in dataItems)
                {
                    serializer.Serialize(file, item);
                }

                //close json
                file.Write(']');
                await file.FlushAsync();
            }
        }
    }
}