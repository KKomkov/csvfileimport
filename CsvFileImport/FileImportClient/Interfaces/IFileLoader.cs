﻿using System.Threading.Tasks;

namespace FileImportClient
{
    public interface IFileLoader
    {
        Task UploadAsync(string filePath);
    }
}