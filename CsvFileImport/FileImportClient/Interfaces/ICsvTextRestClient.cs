﻿using System;
using System.Threading.Tasks;

namespace FileImportClient
{
    public interface ICsvTextRestClient
    {
        Task<Guid> StartFileLoadAsync(string textPart);

        Task<Guid> ContinueFileLoadAsync(Guid fileId, string textPart);
    }
}