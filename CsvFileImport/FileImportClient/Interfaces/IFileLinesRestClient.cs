﻿using System;
using System.Threading.Tasks;

namespace FileImportClient
{
    public interface IFileLinesRestClient
    {
        Task<Guid> ContinueFileLoadAsync(Guid fileId, string[] textLines);

        Task<Guid> StartFileLoadAsync(string[] textLines);
    }
}