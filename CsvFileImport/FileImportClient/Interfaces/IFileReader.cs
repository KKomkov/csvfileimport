﻿using System.Collections.Generic;

namespace FileImportClient
{
    public interface IFileReader
    {
        IEnumerable<string[]> GetTextPart(string filePath);

        IEnumerable<string> ReadFileBlock(string filePath);
    }
}