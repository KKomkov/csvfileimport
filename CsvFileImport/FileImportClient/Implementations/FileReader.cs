﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Text;

namespace FileImportClient
{
    public class FileReader : IFileReader
    {
        protected const int bufferSize = 0x1000; //4 kb

        public FileReader()
        {
        }

        public IEnumerable<string[]> GetTextPart(string filePath)
        {
            var fileText = ReadFileBlock(filePath);

            string[] textLines = null;

            string prevIterationTail = string.Empty;
            string lastLine = string.Empty;

            foreach (string block in fileText)
            {
                textLines = block.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                lastLine = textLines[textLines.Length - 1];
                //correction last broken line;
                if (lastLine[lastLine.Length - 1] == '\r' || lastLine[lastLine.Length - 1] == '\n')
                {
                    prevIterationTail = string.Empty;
                }
                else
                {
                    prevIterationTail = lastLine;
                    Array.Resize(ref textLines, textLines.Length - 1);
                }

                yield return textLines;
            }
            yield break;
        }

        public IEnumerable<string> ReadFileBlock(string filePath)
        {
            int offset = 0x0; //
            int elementCount = 0;

            // Create the memory-mapped file.
            string result;
            using (var mmf = MemoryMappedFile.CreateFromFile(filePath, FileMode.Open))
            {
                using (var accessor = mmf.CreateViewAccessor())
                {
                    byte[] contentArray = new byte[bufferSize];

                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; offset < accessor.Capacity;)
                    {
                        elementCount = contentArray.Length;
                        int readedBytes = accessor.ReadArray<byte>(offset, contentArray, 0, elementCount);

                        result = Encoding.UTF8.GetString(contentArray);

                        offset += readedBytes;
                        i++;
                        yield return result;
                    }
                    yield break;
                }
            }
        }
    }
}