﻿using RestSharp;
using System;
using System.Threading.Tasks;

namespace FileImportClient.Implementations
{
    internal class CsvTextRestClient : ICsvTextRestClient
    {
        private readonly string hostUrl = "http://localhost:50615/api/CsvText";

        public async Task<Guid> StartFileLoadAsync(string textPart)
        {
            try
            {
                var client = new RestSharp.RestClient(hostUrl);
                var request = new RestSharp.RestRequest(RestSharp.Method.POST);
                request.AddJsonBody(textPart);

                var response = await client.ExecuteTaskAsync<Guid>(request);
                if (response.IsSuccessful && response.StatusCode == System.Net.HttpStatusCode.OK)
                    return response.Data;
                else
                    throw new Exception("Failed start fileLoad:" + response.ErrorMessage, response.ErrorException);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<Guid> ContinueFileLoadAsync(Guid fileId, string textPart)
        {
            try
            {
                var client = new RestClient($"{hostUrl}/{fileId}");
                var request = new RestRequest(Method.PUT);

                request.AddJsonBody(textPart);

                var response = await client.ExecuteTaskAsync<Guid>(request);
                if (response.IsSuccessful || response.StatusCode == System.Net.HttpStatusCode.OK)
                    return response.Data;
                else
                    throw new Exception("Failed Continue load");
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}