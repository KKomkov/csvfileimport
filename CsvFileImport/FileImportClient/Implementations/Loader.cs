﻿using System;
using System.Threading.Tasks;

namespace FileImportClient
{
    public class Loader : IFileLoader
    {
        private IFileReader _reader;
        private ICsvTextRestClient _restClient;

        public Loader(IFileReader fileReader, ICsvTextRestClient rest)
        {
            _reader = fileReader;
            _restClient = rest;
        }

        public async Task UploadAsync(string filePath)
        {
            bool IsStartLoad = true;
            var textParts = _reader.ReadFileBlock(filePath);

            Guid continueLoadId = Guid.Empty;

            foreach (string part in textParts)
            {
                if (IsStartLoad)
                {
                    IsStartLoad = false;
                    Console.WriteLine($"startfileLoad continueLoadId:{continueLoadId}");
                    continueLoadId = await _restClient.StartFileLoadAsync(part);
                }
                else
                {
                    Console.WriteLine($"before continuefileLoad continueLoadId:{continueLoadId}");
                    continueLoadId = await _restClient.ContinueFileLoadAsync(continueLoadId, part);
                    Console.WriteLine($"after  continuefileLoad continueLoadId:{continueLoadId}");
                }
            }
        }
    }
}