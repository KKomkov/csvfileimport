﻿using FileImportClient.Implementations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace FileImportClient
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            IConfiguration config = new ConfigurationBuilder()
                                    .AddJsonFile("appsettings.json", true, true)
                                    .Build();

            var collection = new ServiceCollection();

            collection.AddSingleton(typeof(ICsvTextRestClient), typeof(CsvTextRestClient));
            collection.AddSingleton(typeof(IFileReader), typeof(FileReader));
            collection.AddSingleton(typeof(IFileLoader), typeof(Loader));

            IServiceProvider serviceProvider = collection.BuildServiceProvider();

            Console.WriteLine("Start");
            await Start(serviceProvider, config);

            if (serviceProvider is IDisposable)
            {
                ((IDisposable)serviceProvider).Dispose();
            }

            Console.WriteLine("File Uploaded");
            Console.ReadLine();
        }

        private static async Task Start(IServiceProvider serviceProvider, IConfiguration config)
        {
            string filePath = config.GetValue<string>("sourceFile:name");
            IFileLoader loader = serviceProvider.GetService<IFileLoader>();
            await loader.UploadAsync(filePath);
        }
    }
}