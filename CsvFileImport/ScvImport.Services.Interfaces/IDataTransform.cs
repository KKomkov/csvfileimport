﻿namespace CsvImport.Services.Interfaces
{
    public interface IDataTransform<TSource, TDestination, TCommonData>
    {
        TDestination TransformModel(TSource src, TCommonData commonData);
    }
}