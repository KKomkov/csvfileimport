﻿using CsvImport.Service.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CsvImport.Store.Interfaces
{
    public interface IStore
    {
        Task SaveAsync<T>(FileImportInfo fileInfo, IEnumerable<T> dataItems);
    }
}