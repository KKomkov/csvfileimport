﻿using System;
using System.Threading.Tasks;

namespace CsvImport.Services.Interfaces
{
    public interface ITextFileLoad
    {
        Task<Guid> ContinueLoadAsync(Guid continueId, string textPart);

        Task<Guid> StartLoadAsync(string textPart);
    }
}