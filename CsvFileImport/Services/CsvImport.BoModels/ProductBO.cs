﻿namespace CsvImport.BoModels
{
    public class ProductBO
    {
        public string Key { get; set; }
        public string ArtikelCode { get; set; }
        public ColorCodeBO ColorCode { get; set; }
        public string Description { get; set; }
        public string Price { get; set; }
        public string DiscountPrice { get; set; }
        public DeliveryBO DeliveredIn { get; set; }
        public string Q1 { get; set; }
        public string Size { get; set; }
        public ColorBO Color { get; set; }
    }
}