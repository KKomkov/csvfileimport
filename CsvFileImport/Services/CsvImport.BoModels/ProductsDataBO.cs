﻿using System.Collections.ObjectModel;

namespace CsvImport.BoModels
{
    public class ProductsDataBO
    {
        public Collection<ColorBO> Colors { get; } = new Collection<ColorBO>();
        public Collection<ColorCodeBO> ColorCodes { get; } = new Collection<ColorCodeBO>();
        public Collection<DeliveryBO> DeliveryTerms { get; } = new Collection<DeliveryBO>();
        public Collection<ProductBO> Products { get; } = new Collection<ProductBO>();
    }
}