﻿using AutoMapper;
using CsvImport.BoModels;
using CsvImport.CommonModels.InputModels;

namespace CsvImport.Mapping
{
    public class DataTransformMappingProfile : Profile
    {
        public DataTransformMappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<RowLO, ProductBO>()
                .ForMember(dest => dest.Color, opt => opt.Ignore())
                .ForMember(dest => dest.ColorCode, opt => opt.Ignore())
                .ForMember(dest => dest.DeliveredIn, opt => opt.Ignore());
        }
    }
}