﻿using AutoMapper;
using CsvImport.BoModels;
using CsvImport.CommonModels.InputModels;
using CsvImport.Services.Interfaces;
using System;
using System.Linq;

namespace DataTransformSevice
{
    public class DataTransform : IDataTransform<RowLO, ProductBO, ProductsDataBO>
    {
        private readonly IMapper _mapper;

        public DataTransform(IMapper mapper)
        {
            _mapper = mapper;
        }

        public ProductBO TransformModel(RowLO src, ProductsDataBO commonData)
        {
            if (commonData == null)
                commonData = new ProductsDataBO();
            ProductBO newProduct = null;
            try
            {
                newProduct = _mapper.Map<ProductBO>(src);

                newProduct.Color = GetColor(src.Color, commonData);
                newProduct.ColorCode = GetColorCode(src.ColorCode, commonData);
                newProduct.DeliveredIn = GetDeliveryIn(src.DeliveredIn, commonData);
            }
            catch (Exception ex)
            {
                throw;
            }
            commonData.Products.Add(newProduct);
            return newProduct;
        }

        //Todo: use method in mapper
        public static ColorBO GetColor(string colorName, ProductsDataBO commonData)
        {
            var color = commonData.Colors.Where(x => x.Code.Equals(colorName, System.StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

            if (color == null)
            {
                color = new ColorBO() { Code = colorName };

                commonData.Colors.Add(color);
            }

            return color;
        }

        public static ColorCodeBO GetColorCode(string colorCode, ProductsDataBO commonData)
        {
            var colorCodeItem = commonData.ColorCodes.Where(x => x.Code.Equals(colorCode, System.StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

            if (colorCodeItem == null)
            {
                colorCodeItem = new ColorCodeBO() { Code = colorCode };

                commonData.ColorCodes.Add(colorCodeItem);
            }

            return colorCodeItem;
        }

        public static DeliveryBO GetDeliveryIn(string DeliveryIn, ProductsDataBO commonData)
        {
            var DeliveryInItem = commonData.DeliveryTerms.Where(x => x.Code.Equals(DeliveryIn, System.StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

            if (DeliveryInItem == null)
            {
                DeliveryInItem = new DeliveryBO() { Code = DeliveryIn };

                commonData.DeliveryTerms.Add(DeliveryInItem);
            }

            return DeliveryInItem;
        }
    }
}