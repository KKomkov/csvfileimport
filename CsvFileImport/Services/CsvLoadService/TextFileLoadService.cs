﻿using CsvImport.BoModels;
using CsvImport.CommonModels.InputModels;
using CsvImport.Service.Models;
using CsvImport.Services.Interfaces;
using CsvImport.Store.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TinyCsvParser;
using TinyCsvParser.Mapping;

namespace CsvImport.DataLoadService
{
    //TODO:return result object with error message, Is critical ,fileId
    public class TextFileLoadService : ITextFileLoad
    {
        //private IRowParser _parser = null;
        private IEnumerable<IStore> _recipientStores;

        private IDataTransform<RowLO, ProductBO, ProductsDataBO> _dataTransform;
        private IMemoryCache _memoryCache;
        private readonly string[] _rowSeparators = new[] { "\n", "\r" };
        private readonly char _columnSepartor = ',';

        public TextFileLoadService(IMemoryCache memoryCache, IDataTransform<RowLO, ProductBO, ProductsDataBO> dataTransform, IEnumerable<IStore> stores)
        {
            //_parser = parser;
            _recipientStores = stores;
            _memoryCache = memoryCache;
            _dataTransform = dataTransform;
        }

        public async Task<Guid> StartLoadAsync(string textPart)
        {
            //// validate input data
            if (string.IsNullOrEmpty(textPart))
                throw new Exception("Text data is empty");

            //save header
            Guid fileId = Guid.NewGuid();

            AddContinueLoadId(fileId, fileId);
            ////save data to store
            await ContinueLoadAsync(fileId, textPart);
            return fileId;
        }

        public async Task<Guid> ContinueLoadAsync(Guid continueLoadId, string textPart)
        {
            //validate input data
            //parse each line to Logical object(s)
            //save logical objects

            if (string.IsNullOrEmpty(textPart))
                return continueLoadId;

            Guid fileId = GetFileId(continueLoadId);

            bool isLastRowFail = true;
            int maxFailedRowNum = -1;

            var records = ParseCsv(textPart);
            var correctRecords = records.AsParallel()
                                        .Where(r => r.IsValid)
                                        .Select(r => r.Result);

            var productsData = new ProductsDataBO();

            correctRecords.ForAll((x) => { _dataTransform.TransformModel(x, productsData); });

            // var allrows = correctRecords.Select(x => x).ToList();
            // isLastRowFail = records.AsParallel().Any(r => !r.IsValid);//

            FileImportInfo fileInfo = new FileImportInfo() { fileLoadId = fileId };

            //Save rowLo
            //await SaveToStore<RowLO>(fileInfo, correctRecords);
            await SaveToStore<ProductBO>(fileInfo, productsData.Products);

            Guid nextContinueLoadId = Guid.NewGuid();
            AddContinueLoadId(nextContinueLoadId, fileId);
            return nextContinueLoadId;
        }

        protected ParallelQuery<CsvMappingResult<RowLO>> ParseCsv(string textPart)
        {
            CsvReaderOptions readerOptions = new CsvReaderOptions(_rowSeparators);
            CsvParserOptions parserOptions = new CsvParserOptions(true, _columnSepartor);
            var csvParser = new CsvParser<RowLO>(parserOptions, new CsvMap.CsvRowMapping());

            return csvParser.ReadFromString(readerOptions, textPart);
        }

        protected async Task SaveToStore<T>(FileImportInfo fileInfo, IEnumerable<T> rows)
        {
            foreach (IStore store in _recipientStores)
            {
                await store.SaveAsync(fileInfo, rows);
            }
        }

        protected readonly TimeSpan CasheTTL = TimeSpan.FromMinutes(5);
        private const string continueLoadPrefix = "continueId:";

        private string ContinueLoadKey(Guid continueId) => $"{continueLoadPrefix}{continueId}";

        protected void AddContinueLoadId(Guid continueId, Guid fileId)
        {
            _memoryCache.Set<Guid>(ContinueLoadKey(continueId), fileId, CasheTTL);
        }

        protected Guid GetFileId(Guid continueId)
        {
            try
            {
                Guid result = _memoryCache.Get<Guid>(ContinueLoadKey(continueId));
                return result;
            }
            catch
            {
                throw new Exception($"File load continueId:{continueId} not found");
            }
        }
    }
}