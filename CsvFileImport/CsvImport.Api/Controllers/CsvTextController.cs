﻿using CsvImport.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace CsvImport.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CsvTextController : ControllerBase
    {
        private ITextFileLoad _fileService;

        public CsvTextController(ITextFileLoad fileService)
        {
            _fileService = fileService;
        }

        // POST: api/FileText
        /// <summary>Start file load with this method.
        /// Path first N lines of a file.
        /// After it continue file load with api/File/id
        /// </summary>
        /// <param name="text">First N text lines of a file</param>
        /// <returns> Id (Guid) of fileLoad.
        /// Path this id into "api/File/{id}" to continue file load with next file part.</returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> StartLoad([FromBody] string textPart)
        {
            try
            {
                return await _fileService.StartLoadAsync(textPart);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                if (ex.InnerException != null)
                    message += "Internal Ex:" + ex.InnerException.Message;

                return BadRequest(message);
            }
        }

        // PUT: api/FileText/5
        ///<summary>Continue file load with this method; Do not path first file line to this method.
        /// </summary>
        /// <param name="id">Id of fileload that you get from POST: api/File </param>
        /// <param name="lines">Part of N lines of a file.</param>
        /// <returns> Id of part load use it to continue load with next file part.</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult<Guid>> ContinueLoad(Guid id, [FromBody] string textPart)
        {
            try
            {
                //
                return await _fileService.ContinueLoadAsync(id, textPart);
            }
            catch (Exception ex)
            {
                return BadRequest($"Failed load file part { ex.Message}");
            }
        }
    }
}