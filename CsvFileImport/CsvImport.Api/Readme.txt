﻿Backend - C# .NET Core web API
1.  Import the file via web API, expect the file to be very large in production
2.  Transform the data into a logical model
3.  Store the data (two locations - a Database and JSON file)
	o   Database (can be either MS SQL or MongoDB - you choose)
	o   JSON file on the disk
 
Criteria:
Your implementation properly follows all SOLID principles.

To improve:
- One project for full solution. This should be split out in multiple projects.
- Instead of accepting a file with CSV data the API only accept lines.
- The database is flat and does not contain a logical model.
- Using reflection while your database model
and domain models are already defined. This only adds overhead.

Good: 
- IStore for both data store implementations
- "Layers" exist. Even though all is one project. (API => Business => Dal)
