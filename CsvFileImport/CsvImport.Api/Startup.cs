﻿using AutoMapper;
using CsvImport.BoModels;
using CsvImport.CommonModels.InputModels;
using CsvImport.DataLoadService;
using CsvImport.Services.Interfaces;
using CsvImport.Store.Interfaces;
using CsvImport.Store.JsonStore;
using CsvImport.Store.Mssql;
using DataTransformSevice;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using MssqlEfStore.Model;
using Swashbuckle.AspNetCore.Swagger;
using System.IO;

namespace CsvImport
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<MssqlDbContext>(options =>
            {
                options.UseSqlServer(Configuration["ConnectionStrings:DefaultConnection"]);
            });

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new CsvImport.Mapping.DataTransformMappingProfile());
                mc.AddProfile(new MssqlEfStore.Mapping.MssqlMappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddTransient(typeof(ITextFileLoad), typeof(TextFileLoadService));
            services.AddTransient(typeof(IDataTransform<RowLO, ProductBO, ProductsDataBO>), typeof(DataTransform));

            //add store services
            services.AddTransient(typeof(IStore), typeof(JsonStore));
            services.AddTransient(typeof(IStore), typeof(MssqlStore));

            services.AddMvc()
                    .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Csv file import API", Version = "version 1" });

                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "CsvFileImport.xml");
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
            app.UseMvc();
        }
    }
}